module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'scope-case': [2, 'always', 'upper-case'],
    'type-enum': [
      2,
      'always',
      [
        'fix',
        'chore',
        'feat',
        'docs',
        'style',
        'refactor',
        'perf',
        'test',
        'revert',
      ],
    ],
    'subject-case': [2, 'always', ['lower-case']],
  },
};
