import { combineReducers } from 'redux';
import { QuizState } from './quiz/types';
import quizReducer from './quiz/reduces';

export interface RootState {
  quizes: QuizState;
}

const rootReducer = combineReducers<RootState>({
  quizes: quizReducer,
});

export default rootReducer;
