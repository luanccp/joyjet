import { createStore, Store } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import rootReducer, { RootState } from './rootReducer';
import reactotron from '../config/ReactotronConfig';

const persistConfig = {
  key: 'quiz',
  storage,
  // blacklist: ['quizes'], // navigation will not be persisted
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store: Store<RootState> = reactotron.createEnhancer
  ? createStore(persistedReducer, reactotron.createEnhancer())
  : createStore(persistedReducer);

export default store;
export const persistor = persistStore(store);
