import produce from 'immer';
import { QuizState, QuizActions } from './types';

const initialState: QuizState = {
  quizes: [],
};

export default function quizReducer(state = initialState, action: any) {
  switch (action.type) {
    case QuizActions.ADD_QUIZ:
      return produce(state, (draftState) => {
        draftState.quizes.push(action.quiz);
      });
    case QuizActions.ADD_QUESTION:
      return produce(state, (draftState) => {
        const quiz = draftState.quizes.find(
          (quizItem) => quizItem.id === action.quizId
        );

        if (quiz) {
          quiz.questions.push(action.question);
        }
      });
    case QuizActions.ADD_ANSWER:
      return produce(state, (draftState) => {
        const quiz = draftState.quizes.find(
          (quizItem) => quizItem.id === action.quizId
        );
        if (quiz) {
          const question = quiz.questions.find(
            (questionItem) => questionItem.id === action.questionId
          );

          if (question) {
            question.answers.push(action.answer);
          }
        }
      });
    case QuizActions.SAVE_QUESTION:
      return produce(state, (draftState) => {
        const quiz = draftState.quizes.find(
          (quizItem) => quizItem.id === action.quizId
        );
        if (quiz) {
          const question = quiz.questions.find(
            (questionItem) => questionItem.id === action.questionId
          );

          if (question) {
            question.description = action.questionDescription;
            console.log('R-rightanswer: ', action.rightAnswer);
            question.rightAnswer = action.rightAnswer;
            const answer = question.answers.find(
              (answerItem) => answerItem.id === action.answerId
            );
            if (answer) {
              answer.description = action.answerDescription;
            }
          }
        }
      });
    default:
      return state;
  }
}
