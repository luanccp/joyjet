import Quiz from '../../interfaces/Quiz';
import Question from '../../interfaces/Question';
import Answer from '../../interfaces/Answer';

export enum QuizActions {
  ADD_QUIZ = 'quiz/ADD_QUIZ',
  ADD_QUESTION = 'quiz/ADD_QUESTION',
  ADD_ANSWER = 'quiz/ADD_ANSWER',
  SAVE_QUESTION = 'quiz/SAVE_QUESTION',
}

/**
 * INTERFACES (state)
 */

export interface QuizState {
  quizes: Quiz[];
}

/**
 * ACTIONS
 */

interface AddQuizAction {
  type: QuizActions.ADD_QUIZ;
  quiz: Quiz;
}

interface AddQuestionAction {
  type: QuizActions.ADD_QUESTION;
  question: Question;
  quizId: string;
}

interface AddAnswerAction {
  type: QuizActions.ADD_ANSWER;
  questionId: string;
  quizId: string;
  answer: Answer;
}

interface SaveQuestionAction {
  type: QuizActions.SAVE_QUESTION;
  questionId: string;
  quizId: string;
  answerId: string;
  answerDescription: string;
  questionDescription: string;
  rightAnswer: string;
}

export type QuizActionTypes =
  | AddQuizAction
  | AddQuestionAction
  | AddAnswerAction
  | SaveQuestionAction;
