import { QuizActions, QuizActionTypes } from './types';
import Quiz from '../../interfaces/Quiz';
import Question from '../../interfaces/Question';
import Answer from '../../interfaces/Answer';

export function addQuiz(quiz: Quiz): QuizActionTypes {
  return {
    type: QuizActions.ADD_QUIZ,
    quiz,
  };
}

export function addQuestion(
  question: Question,
  quizId: string
): QuizActionTypes {
  return {
    type: QuizActions.ADD_QUESTION,
    quizId,
    question,
  };
}

export function addAnswer(
  answer: Answer,
  questionId: string,
  quizId: string
): QuizActionTypes {
  return {
    type: QuizActions.ADD_ANSWER,
    answer,
    questionId,
    quizId,
  };
}

export function saveQuestion(
  answerId: string,
  questionId: string,
  quizId: string,
  answerDescription: string,
  questionDescription: string,
  rightAnswer: string
): QuizActionTypes {
  return {
    type: QuizActions.SAVE_QUESTION,
    answerId,
    questionId,
    quizId,
    answerDescription,
    questionDescription,
    rightAnswer,
  };
}
