import Answer from './Answer';

export default interface Question {
  id: string;
  description: string;
  rightAnswer: string;
  answers: Answer[];
}
