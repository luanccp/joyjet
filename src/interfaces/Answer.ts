export default interface Answer {
  id: string;
  description: string;
}
