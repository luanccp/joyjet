import Question from './Question';

export default interface Quiz {
  id: string;
  questions: Question[];
}
