/* eslint-disable no-alert */
import React, { useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import {
  Container,
  Answer,
  RadioAnswer,
  ActionSection,
  QuestionSection,
} from './styles';
import Button from '../../components/Button';
import { RootState } from '../../store/rootReducer';
import { QuizState } from '../../store/quiz/types';
import Question from '../../interfaces/Question';
import Quiz from '../../interfaces/Quiz';

const AnswerQuiz: React.FC = () => {
  const { quizId, questionId } = useParams();

  const history = useHistory();

  const [questionIndex, setQuestionIndex] = useState<number>(0);
  const [quiz, setQuiz] = useState<Quiz>();
  const [question, setQuestion] = useState<Question>();
  const { quizes } = useSelector<RootState, QuizState>((state) => state.quizes);
  const [selectedAnswer, setselectedAnswer] = useState('');

  useEffect(() => {
    const foundQuiz = quizes.find((quizItem) => quizItem.id === quizId);
    setQuiz(foundQuiz);
    if (foundQuiz) {
      const foundQuestion = foundQuiz.questions.find(
        (questionItem, questionItemIndex) => {
          if (questionItem.id === questionId) {
            setQuestionIndex(questionItemIndex);
            return true;
          }

          return false;
        }
      );
      setQuestion(foundQuestion);
    }
  }, [quizes, questionId, quizId, question, questionIndex, quiz]);

  const handleCheckAnswer = useCallback(() => {
    if (!selectedAnswer) {
      alert('You must select one answer');
      return;
    }
    if (question?.rightAnswer === selectedAnswer) {
      alert('Yay! You are right!');
      history.push(
        `/answer-quiz/${quizId}/${quiz?.questions[questionIndex + 1].id}`
      );
    } else {
      alert('Ops, try again!');
    }
  }, [question, selectedAnswer, history, questionIndex, quiz, quizId]);

  return (
    <>
      <Navbar>QUIT</Navbar>
      <Container>
        {question?.description === '' ? (
          <h2>Thank you for answer the quiz!</h2>
        ) : (
          <>
            <QuestionSection>
              <strong>Question {questionIndex + 1 || 1}</strong>
              <p>{question?.description}</p>
            </QuestionSection>
            {question?.answers.map((ans) => (
              <Answer key={ans.id}>
                <RadioAnswer>
                  <input
                    type="radio"
                    name="right"
                    value={ans.id}
                    onChange={(event) => setselectedAnswer(event.target.value)}
                  />
                </RadioAnswer>
                <p>{ans.description}</p>
              </Answer>
            ))}

            <ActionSection>
              <Button type="button" onClick={handleCheckAnswer}>
                Next question
              </Button>
            </ActionSection>
          </>
        )}
      </Container>
    </>
  );
};

export default AnswerQuiz;
