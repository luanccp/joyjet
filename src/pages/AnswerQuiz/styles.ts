import styled, { keyframes } from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: 30px 90px 0 90px;

  strong {
    font-weight: bold;
    margin-bottom: 4px;
  }

  p {
    font-weight: 400;
    margin-bottom: 8px;
  }
`;

export const HeaderAnswers = styled.div`
  display: flex;
  width: 100%;

  justify-content: space-between;
  align-items: center;

  padding: 38px 0 4px 0;

  span {
    width: 50px;
    text-align: center;
    font-size: 12px;
  }
  div {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    button {
      width: 36px;
      height: 36px;
      border-radius: 50%;

      margin-left: 6px;
      display: flex;
      justify-content: center;
      align-items: center;

      border: none;
    }
  }
`;

const radioAnimation = keyframes`

  from{
    transform: scale(1, 1);
  }
  30%{
    transform: scale(1.25, 0.75);
  }
  40%{
    transform: scale(0.75, 1.25);
  }
  50%{
    transform: scale(1.15, 0.85);
  }
  65%{
    transform: scale(.95, 1.05);
  }
  75%{
    transform: scale(1.05, .95);
  }
  to{
    transform: scale(1, 1);;
  }
`;

export const Answer = styled.div`
  display: flex;
  justify-content: flex-start;
  width: 100%;
`;

export const RadioAnswer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 50px;

  input[type='text'] {
    width: inherit;
  }

  input[type='radio'] {
    position: relative;
    width: 28px;
    height: 28px;
    cursor: pointer;
  }
  input[type='radio']:checked {
    animation: ${radioAnimation} 0.6s ease;
  }
`;

export const ActionSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 32px 0;
`;

export const QuestionSection = styled.div`
  margin: 26px 0;

  strong {
    font-size: 22px;
    line-height: 2;
  }
  p {
    font-size: 20px;
  }
`;
