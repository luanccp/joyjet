import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: center;
  align-items: center;
  min-height: 80vh;

  strong {
    font-weight: bold;
    margin-bottom: 4px;
  }

  p {
    font-weight: 400;
    margin-bottom: 8px;
  }

  ul {
    list-style: none;

    a {
      text-decoration: none;
      color: inherit;

      &:active {
        color: inherit;
      }
      &:visited {
        color: inherit;
      }
    }

    li {
      padding: 12px;
      text-decoration: none;

      transition: border-left 0.2s;
      &:hover {
        border-left: solid 4px #ff9000;
      }
      &:active {
        border-left: solid 14px #ff9000;
      }
    }
  }
`;
