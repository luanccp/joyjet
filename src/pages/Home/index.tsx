import React, { useCallback } from 'react';
import { useHistory, Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuid } from 'uuid';
import Navbar from '../../components/Navbar';
import { Container } from './styles';
import Button from '../../components/Button';
import Quiz from '../../interfaces/Quiz';
import { addQuiz } from '../../store/quiz/actions';
import { RootState } from '../../store/rootReducer';
import { QuizState } from '../../store/quiz/types';

const Home: React.FC = () => {
  const { quizes } = useSelector<RootState, QuizState>((state) => state.quizes);
  const dispatch = useDispatch();
  const history = useHistory();
  const handleCreateQuiz = useCallback(() => {
    const quiz: Quiz = {
      id: uuid(),
      questions: [
        {
          id: uuid(),
          description: '',
          rightAnswer: '',
          answers: [],
        },
      ],
    };
    dispatch(addQuiz(quiz));

    history.push(`/edit-quiz/${quiz.id}/${quiz.questions[0].id}`);
  }, [dispatch, history]);

  return (
    <>
      <Navbar />
      <Container>
        {quizes.length >= 1 ? (
          <ul>
            {quizes.map((quiz) => (
              <Link
                key={quiz.id}
                to={`/answer-quiz/${quiz.id}/${quiz.questions[0].id}`}
              >
                <li>
                  <strong>Quiz : </strong>
                  {quiz.id}
                </li>
              </Link>
            ))}
          </ul>
        ) : (
          <>
            <strong>Hello, you `:)`</strong>
            <p>Let&apos;s play?</p>
          </>
        )}
        <Button type="button" onClick={handleCreateQuiz}>
          Create quiz
        </Button>
      </Container>
    </>
  );
};

export default Home;
