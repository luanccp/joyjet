/* eslint-disable no-alert */
import React, { useCallback, useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form } from '@unform/web';
import { useParams, useHistory } from 'react-router-dom';
import { v4 as uuid } from 'uuid';
import { FormHandles } from '@unform/core';
import Navbar from '../../components/Navbar';
import {
  Container,
  HeaderAnswers,
  AnswerSection,
  RadioAnswer,
  ActionSection,
} from './styles';
import Button from '../../components/Button';
import Input from '../../components/Input';
import { RootState } from '../../store/rootReducer';
import Quiz from '../../interfaces/Quiz';
import { addQuestion, addAnswer, saveQuestion } from '../../store/quiz/actions';
import { QuizState } from '../../store/quiz/types';
import Question from '../../interfaces/Question';
import Answer from '../../interfaces/Answer';

const EditQuiz: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const formRef = useRef<FormHandles>(null);
  const { quizId, questionId } = useParams();

  const [questionIndex, setQuestionIndex] = useState<number>(0);
  const [quiz, setQuiz] = useState<Quiz>();
  const [question, setQuestion] = useState<Question>();
  const { quizes } = useSelector<RootState, QuizState>((state) => state.quizes);
  const [rightAnswer, setRightAnswer] = useState('');

  useEffect(() => {
    const foundQuiz = quizes.find((quizItem) => quizItem.id === quizId);
    setQuiz(foundQuiz);
    if (foundQuiz) {
      const foundQuestion = foundQuiz.questions.find(
        (questionItem, questionItemIndex) => {
          if (questionItem.id === questionId) {
            setQuestionIndex(questionItemIndex);
            return true;
          }

          return false;
        }
      );
      setQuestion(foundQuestion);
    }
  }, [quizes, questionId, quizId, question, questionIndex, quiz]);

  const handleCreateAnswer = useCallback(() => {
    const answer: Answer = {
      id: uuid(),
      description: '',
    };

    dispatch(addAnswer(answer, questionId, quizId));
  }, [dispatch, questionId, quizId]);

  const handleCreateQuestion = useCallback(
    (data) => {
      if (data.questionDescription === '') {
        alert('You must add a question description.');
        return;
      }

      if (data.answer === undefined || data.answer.length === 0) {
        alert('You must have at least one answer.');
        return;
      }

      if (rightAnswer === '') {
        alert('You must add a right answer.');
        return;
      }
      if (question) {
        if (question?.answers.length < 4) {
          alert('You must add at least 4 answers.');
          return;
        }
        for (let i = 0; i < question?.answers.length; i += 1) {
          const answerId = question.answers[i].id;
          const keys = Object.keys(data.answer);

          if (data.answer[keys[i]] === '') {
            alert('All your answers must to have a description');
            return;
          }
          dispatch(
            saveQuestion(
              answerId,
              questionId,
              quizId,
              data.answer[keys[i]],
              data.questionDescription,
              rightAnswer
            )
          );
        }
      }
      const nextQuestion: Question = {
        id: uuid(),
        description: '',
        rightAnswer,
        answers: [],
      };
      dispatch(addQuestion(nextQuestion, quizId));
      formRef.current?.clearField('questionDescription');
      setRightAnswer('');
      history.push(`/edit-quiz/${quizId}/${nextQuestion.id}`);
    },
    [dispatch, history, quizId, question, rightAnswer, questionId]
  );

  return (
    <>
      <Navbar>END QUIZ</Navbar>
      <Container>
        <h1>Quiz #{quiz?.id}</h1>
        <p>Total questions {quiz?.questions.length}</p>
        <Form ref={formRef} onSubmit={handleCreateQuestion}>
          <strong>Question {questionIndex + 1}</strong>
          <Input
            type="text"
            name="questionDescription"
            placeholder={question?.description || 'type your question'}
          />
          <HeaderAnswers>
            <span>right answer</span>
            <div>
              <p>Add answer</p>
              <button type="button" onClick={handleCreateAnswer}>
                +
              </button>
            </div>
          </HeaderAnswers>
          {question?.answers.map((answer) => {
            return (
              <AnswerSection key={answer.id.toString()}>
                <RadioAnswer>
                  <input
                    type="radio"
                    name="right"
                    value={answer.id}
                    onChange={(event) => setRightAnswer(event.target.value)}
                  />
                </RadioAnswer>
                <Input
                  type="text"
                  name={`answer[${answer.id}]`}
                  placeholder={answer.description || 'type here'}
                />
              </AnswerSection>
            );
          })}
          <ActionSection>
            <Button
              type="button"
              typeButton="cancel"
              onClick={() => history.push('/')}
            >
              Cancel question
            </Button>
            <Button type="submit">Create question</Button>
          </ActionSection>
        </Form>
      </Container>
    </>
  );
};

export default EditQuiz;
