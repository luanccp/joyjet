import React from 'react';
import { Switch } from 'react-router-dom';

import Route from './Route';
import Home from '../pages/Home';
import EditQuiz from '../pages/EditQuiz';
import AnswerQuiz from '../pages/AnswerQuiz';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/edit-quiz/:quizId/:questionId" exact component={EditQuiz} />
    <Route
      path="/answer-quiz/:quizId/:questionId"
      exact
      component={AnswerQuiz}
    />
  </Switch>
);

export default Routes;
