import React, { ButtonHTMLAttributes } from 'react';
import { Container } from './styles';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  typeButton?: string;
}

const Button: React.FC<ButtonProps> = ({ children, typeButton, ...rest }) => (
  <Container type="button" {...rest} typeButton={typeButton}>
    {children}
  </Container>
);

export default Button;
