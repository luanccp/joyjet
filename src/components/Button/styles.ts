import styled, { css } from 'styled-components';
import { shade } from 'polished';

interface ButtonProps {
  typeButton?: string;
}

export const Container = styled.button<ButtonProps>`
  background: #c4c4c4;
  padding: 14px 20px;
  border-radius: 8px;
  border: 0;
  color: #312e38;
  font-weight: 500;
  margin: 6px;
  transition: background-color 0.2s;

  ${(props) =>
    props.typeButton === 'cancel' &&
    css`
      background: transparent;
      border: 2px solid #dddddd;

      &:hover {
        border-color: ${shade(0.2, '#ff9000')};
        color: ${shade(0.2, '#ff9000')};
      }
    `}

  ${(props) =>
    props.typeButton !== 'cancel' &&
    css`
      &:hover {
        background: ${shade(0.2, '#ff9000')};
      }
    `}
`;
