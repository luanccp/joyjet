import styled from 'styled-components';

export const Container = styled.nav`
  display: flex;
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  padding: 0 20px;

  height: 60px;
  background: lightgray;

  h2 {
    font-size: 20px;
  }

  a {
    font-size: 20px;
    text-decoration: none;
    color: #999;

    &:hover {
      color: #ff9000;
    }
  }

  svg {
    color: #999;
    width: 28px;
    height: 28px;

    transition: width, height 0.2s;

    &:hover {
      color: #ff9000;
    }
    &:active {
      width: 32px;
      height: 32px;
    }
  }
`;
