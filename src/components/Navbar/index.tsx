/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { FaClipboardCheck } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { Container } from './styles';

const Navbar: React.FC = ({ children }) => {
  return (
    <Container>
      <Link to="/">
        <FaClipboardCheck />
      </Link>
      <h2>QUIZ</h2>

      <Link to="/">{children}</Link>
    </Container>
  );
};

export default Navbar;
