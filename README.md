This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Design
I made a Figma to help me guiding in the development process. **[Click here](https://www.figma.com/file/CTZLJgRJi4EJLlZ2pdoAzH/Joyjet-challenge?node-id=0%3A1)** , to see more details.

## How to run

After cloning the repository, in the project directory, you can run:
```bash
yarn
```

After install all dependecies, you can run:
```bash
yarn start
```
If the project not open in your browser, go to http://localhost:3000/

## Project architecture

### components/
Here we have shared components

### config/
I like to use Reactotron to debug my redux process. So I put my configuration files here.

### interfaces/
In this project Answer, Question, and Quiz are treated as entities and their interfaces are here.

### pages/
All pages from the application. Each page has your own components, if this components to started repeat themselves we move them to the components folder.

### routes/
All routes configuration, like paths.

### store/
All redux configuration. In this folder, we have persistent configuration and how are typed our rootReducer. And most importantly, here are types, reduces, and actions from **Quiz**.

### styles/
In this folder, we can set our default style configuration. If we needed to work with themes, the *ThemeProvider* configuration will be put here.


#### Commits
**This project has eslint configured with *Prettier* for code formating. Besides that, was adopted a patter of commits. To guarantee that, *Husky* was adopted. Husky that prevents wrong format for commit messages. The following rules needs to be applied.**

##### Format

```text
type(): subject
```


##### Types

The following types are available:

* **fix**: A new fix.
* **chore**: Changes to the build process or auxiliary tools and libraries such as documentation generation.
* **feat**: A new feature.
* **docs**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (White-space, formatting, missing semi-colons, etc).
* **refactor**: A code change that neither fixes a bug or adds a feature.
* **perf**: A code change that improves performance.
* **test**: Adding missing tests.
* **revert**: Reverting a code.

##### Example

```bash
git commit -m "docs: changed readme"

git commit -m "fix: bug somewere"
```
